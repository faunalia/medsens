<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" simplifyLocal="1" hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1" readOnly="0" version="3.4.7-Madeira" maxScale="-4.65661e-10" styleCategories="AllStyleCategories" simplifyDrawingHints="1" labelsEnabled="0" simplifyAlgorithm="0" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 enableorderby="0" type="RuleRenderer" symbollevels="0" forceraster="0">
    <rules key="{a6809e5d-e250-44ff-afb7-ee36392464f2}">
      <rule label="Very low" symbol="0" filter="&quot;MedSensChe&quot; >= 0.0000 AND &quot;MedSensChe&quot; &lt;= 1.4381" key="{d1110df0-6222-4e34-a061-8577e839de89}"/>
      <rule label="Low" symbol="1" filter="&quot;MedSensChe&quot; > 1.4381 AND &quot;MedSensChe&quot; &lt;= 1.6342" key="{6d102d81-4a77-44cd-a461-bd9953df3ce6}"/>
      <rule label="Moderate" symbol="2" filter="&quot;MedSensChe&quot; > 1.6342 AND &quot;MedSensChe&quot; &lt;= 1.7806" key="{f7300f29-cc7d-48bd-9ed5-903af5664db8}"/>
      <rule label="High" symbol="3" filter="&quot;MedSensChe&quot; > 1.7806 AND &quot;MedSensChe&quot; &lt;= 1.9621" key="{e54dd834-a281-4ee6-baba-52a16fb01b4f}"/>
      <rule label="Very high" symbol="4" filter="&quot;MedSensChe&quot; > 1.9621 AND &quot;MedSensChe&quot; &lt;= 3.0000" key="{5b799a6d-d0fd-44b3-a558-fb573a68d626}"/>
      <rule label="Not assessed" symbol="5" filter="ELSE" key="{23e79664-a8bf-4588-9b9e-9bf7093ed7e4}"/>
    </rules>
    <symbols>
      <symbol name="0" force_rhr="0" alpha="1" type="fill" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="215,25,28,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" force_rhr="0" alpha="1" type="fill" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="253,174,97,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="2" force_rhr="0" alpha="1" type="fill" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,255,192,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="3" force_rhr="0" alpha="1" type="fill" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="166,217,106,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="4" force_rhr="0" alpha="1" type="fill" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="26,150,65,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="5" force_rhr="0" alpha="1" type="fill" clip_to_extent="1">
        <layer locked="0" pass="0" enabled="1" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,255,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property key="dualview/previewExpressions">
      <value>id</value>
      <value>"AreaCode"</value>
    </property>
    <property value="0" key="embeddedWidgets/count"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory labelPlacementMethod="XHeight" penWidth="0" sizeType="MM" penColor="#000000" backgroundAlpha="255" backgroundColor="#ffffff" width="15" minimumSize="0" penAlpha="255" diagramOrientation="Up" scaleDependency="Area" barWidth="5" opacity="1" sizeScale="3x:0,0,0,0,0,0" minScaleDenominator="-4.65661e-10" maxScaleDenominator="1e+08" height="15" enabled="0" lineSizeType="MM" lineSizeScale="3x:0,0,0,0,0,0" rotationOffset="270" scaleBasedVisibility="0">
      <fontProperties style="" description="MS Shell Dlg 2,7.8,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings zIndex="0" linePlacementFlags="18" placement="1" priority="0" obstacle="0" showAll="1" dist="0">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="MPAZONEID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="TYPE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Size">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Zone">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="MSVtot">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="MSVphy">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="MSVchem">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="MSVbio">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="observers">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="observatio">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="taxa">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="area_km2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="MPAZONEID"/>
    <alias index="1" name="" field="TYPE"/>
    <alias index="2" name="" field="Size"/>
    <alias index="3" name="" field="Zone"/>
    <alias index="4" name="" field="Name"/>
    <alias index="5" name="" field="MSVtot"/>
    <alias index="6" name="" field="MSVphy"/>
    <alias index="7" name="" field="MSVchem"/>
    <alias index="8" name="" field="MSVbio"/>
    <alias index="9" name="" field="observers"/>
    <alias index="10" name="" field="observatio"/>
    <alias index="11" name="" field="taxa"/>
    <alias index="12" name="" field="area_km2"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="MPAZONEID"/>
    <default expression="" applyOnUpdate="0" field="TYPE"/>
    <default expression="" applyOnUpdate="0" field="Size"/>
    <default expression="" applyOnUpdate="0" field="Zone"/>
    <default expression="" applyOnUpdate="0" field="Name"/>
    <default expression="" applyOnUpdate="0" field="MSVtot"/>
    <default expression="" applyOnUpdate="0" field="MSVphy"/>
    <default expression="" applyOnUpdate="0" field="MSVchem"/>
    <default expression="" applyOnUpdate="0" field="MSVbio"/>
    <default expression="" applyOnUpdate="0" field="observers"/>
    <default expression="" applyOnUpdate="0" field="observatio"/>
    <default expression="" applyOnUpdate="0" field="taxa"/>
    <default expression="" applyOnUpdate="0" field="area_km2"/>
  </defaults>
  <constraints>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="MPAZONEID" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="TYPE" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="Size" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="Zone" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="Name" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="MSVtot" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="MSVphy" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="MSVchem" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="MSVbio" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="observers" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="observatio" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="taxa" notnull_strength="0"/>
    <constraint constraints="0" exp_strength="0" unique_strength="0" field="area_km2" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="MPAZONEID" desc=""/>
    <constraint exp="" field="TYPE" desc=""/>
    <constraint exp="" field="Size" desc=""/>
    <constraint exp="" field="Zone" desc=""/>
    <constraint exp="" field="Name" desc=""/>
    <constraint exp="" field="MSVtot" desc=""/>
    <constraint exp="" field="MSVphy" desc=""/>
    <constraint exp="" field="MSVchem" desc=""/>
    <constraint exp="" field="MSVbio" desc=""/>
    <constraint exp="" field="observers" desc=""/>
    <constraint exp="" field="observatio" desc=""/>
    <constraint exp="" field="taxa" desc=""/>
    <constraint exp="" field="area_km2" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortExpression="&quot;MSVbio&quot;" sortOrder="1" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" name="MSVtot" width="-1" type="field"/>
      <column hidden="0" name="MSVphy" width="-1" type="field"/>
      <column hidden="0" name="MSVchem" width="-1" type="field"/>
      <column hidden="0" name="MSVbio" width="-1" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
      <column hidden="0" name="observers" width="-1" type="field"/>
      <column hidden="0" name="area_km2" width="-1" type="field"/>
      <column hidden="0" name="taxa" width="-1" type="field"/>
      <column hidden="0" name="observatio" width="-1" type="field"/>
      <column hidden="0" name="MPAZONEID" width="-1" type="field"/>
      <column hidden="0" name="TYPE" width="-1" type="field"/>
      <column hidden="0" name="Size" width="-1" type="field"/>
      <column hidden="0" name="Zone" width="-1" type="field"/>
      <column hidden="0" name="Name" width="-1" type="field"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1">Y:/GIS/RCI</editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath>Y:/GIS/RCI</editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from PyQt4.QtGui import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="AreaCode" editable="1"/>
    <field name="MPAZONEID" editable="1"/>
    <field name="MSVbio" editable="1"/>
    <field name="MSVchem" editable="1"/>
    <field name="MSVphy" editable="1"/>
    <field name="MSVtot" editable="1"/>
    <field name="Name" editable="1"/>
    <field name="Size" editable="1"/>
    <field name="Subz_Zone" editable="1"/>
    <field name="TYPE" editable="1"/>
    <field name="Zone" editable="1"/>
    <field name="area_km2" editable="1"/>
    <field name="id" editable="1"/>
    <field name="observatio" editable="1"/>
    <field name="observations" editable="1"/>
    <field name="observers" editable="1"/>
    <field name="taxa" editable="1"/>
    <field name="taxon" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="AreaCode" labelOnTop="0"/>
    <field name="MPAZONEID" labelOnTop="0"/>
    <field name="MSVbio" labelOnTop="0"/>
    <field name="MSVchem" labelOnTop="0"/>
    <field name="MSVphy" labelOnTop="0"/>
    <field name="MSVtot" labelOnTop="0"/>
    <field name="Name" labelOnTop="0"/>
    <field name="Size" labelOnTop="0"/>
    <field name="Subz_Zone" labelOnTop="0"/>
    <field name="TYPE" labelOnTop="0"/>
    <field name="Zone" labelOnTop="0"/>
    <field name="area_km2" labelOnTop="0"/>
    <field name="id" labelOnTop="0"/>
    <field name="observatio" labelOnTop="0"/>
    <field name="observations" labelOnTop="0"/>
    <field name="observers" labelOnTop="0"/>
    <field name="taxa" labelOnTop="0"/>
    <field name="taxon" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>id</previewExpression>
  <mapTip>MPAZONEID</mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
